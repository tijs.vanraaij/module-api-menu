This module provides an API endpoint to get the category menu as configured in the back office.

The endpoint will return the categories in the order as configured in the category tree by the admin. 
Also, it will only include categories that have both 'is_active'=1 and 'include_in_menu'=1. 

The menu depth can be configured under:
Stores > Configuration > Itc > Api Menu > Max Depth Level

This value should always be > 1 as the primary menu items have level=2  
